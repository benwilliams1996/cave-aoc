#![allow(non_snake_case)]
fn main()
{   
   let height: usize  =  6084;
   let given_X: usize  = 14;
   let given_Y: usize = 709;
   The_Cave(height, given_X, given_Y);
}

fn The_Cave(depth: usize, target_x: usize, target_y: usize) -> ()
{
   
    let mut ER_Index_Vector = vec![]; //store erosion values
    let mut risk_level: u32 = 0;

    for x in 0..=target_x 
    {   
        ER_Index_Vector.push(vec![]);
        for y in 0..=target_y
        {   if x == target_x && y== target_y {break}
            else
            {
                let Geological_Index =  if y == 0
                {
                    x * 16807
                }

                 else if x == 0
                {
                    y * 48271  
                }

                else 
                {
                    ER_Index_Vector[x][y-1] * ER_Index_Vector[x-1][y]
                };

                let Erosion_Level = ((Geological_Index ) + depth) % 20183; 
                 
                risk_level += (Erosion_Level as u32)%3;
                     
                ER_Index_Vector[x].push(Erosion_Level);
        }   
    }//y loop end

  }//x loop end
  println!("The total risk level of the rectangle is: {} ", risk_level);                    

}//function call end
